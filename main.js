"use strict";

function renderList (arr , el = document.body) {

    const list = arr.map(function (arrEl) {
        return `<li>${arrEl}</li>`;
    })

    if ( el.tagName === 'UL' ) {
        el.innerHTML = list.join('');
    } else {
        el.innerHTML = `<ul>${list.join('')}</ul>`;
    }

}

const citiesListEl = document.querySelector('.cities');
const numbersListEl = document.querySelector('.numbers');
// console.log('cities', cities)
renderList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], citiesListEl);
renderList(["1", "2", "3", "sea", "user", 23] , numbersListEl);